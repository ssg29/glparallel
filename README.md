# glParallel

## How to use
There are two ways of running `glParallel`: 1) locally or 2) in a computing cluster via `slurm`.

### running `glParallel` locally
```shell
Rscript run_glParallel.R data/dummy.input.csv 2 2 1 1
```
+ Parameter 1: the location of input file with each row being sample and each column being feature (e.g. gene, protein). NB the last column should be `y` with either 1 (case) or 0 (control).
+ Parameter 2: the number of feature you would like to choose
+ Parameter 3: the number of chunk that you would like to split the input
+ Parameter 4: the number of index for the chunk. NB it should be less than or euqal to the number of chunk (i.e. parameter 3)
+ Parameter 5: the parallel method to use (1: mcapply; 2: foreach) - default 1

The output files will be generated in `result` folder.

### running `glParalle` via `slurm`
```shell
bash submit_jobs.sh
```

You need to set the following variables:
```bash
export NAME="Method1" # any character string you want to use
export INPUT="data\/dummy.input.csv" # the input file (NB, only change the "dummy.input.csv" and leave the backslash as it is.)
export N_V=3 # NO. of features (e.g protein, gene, etc) to select
export N_C=1 # NO. of chunk
export METHOD=3 # 1: mclapply (via fork), 2: foreach (via fork), 3: parLapply (via socket)
```

Also make sure that you change the `SLURM` parameter from the file:`slurm/slurm.skylake.glParallel.template.sh`, e.g
```shell
#SBATCH -A CHARNOCK-JONES-SL3-CPU  # this should be your account
#SBATCH --time=1:00:00 # this instructs that the walltime is 1hr. The script will be killed if it takes >1hr
#SBATCH --qos=INTR # enable this one with the sevrcie level 3 (i.e. SL3) once the wall time is <1hr. This will take a shroter time in the queue.
```

Also from the file above, make sure you enable/disable your `conda` envrionment in case you have your `R` installed via `conda`.
```shell
# conda env by Sung 2023-04-05
source ~/miniconda3/etc/profile.d/conda.sh # https://github.com/conda/conda/issues/8536 
conda activate bioc3.12
```
If the above lines were diabled, it assumes the `Rscript` is the default system when you login. 
To check your version of R try `R --version ` or `which Rscript`.

## Optimism-adjusted C-stat
The following three methods are availabled to correct optimistic estimation of AUC (i.e. optimism-corrected AUC):
+ Bootstrapping: the same number of samples were resampling with replacement (i.e. bootstrapped) and the difference of AUC was recorded between the bootstrapped samples and that the original samples based on the bootstrapped model. This was repeated 1,000 times and the mean difference was subtracted from the original AUC.
```r
adjusted.C<-auc.adjust(data=mat, fit=model, B=1000)
```

+ K-Fold-Cross-validation (KFCV): The above setting was applied in 10-fold cross validation with 100 repetitions.
```r
KFCV<-get_KFCV(x=mat, mc.cores=20)
```

+ Leave-Pair-Out-Cross-Validation (LPOCV) [Gordon Am J Epi 2014](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4108045/): A model was built based on the original samples except one pair of case-and-control, then it was used to predict the outcome of the remaining pair. LPOCV was calculated defined as the proportion of all pairwise combinations in which the predicted probability was greater for the case than for the control.
```r
LPOCV<-get_LPOCV(x=my.mat)
```
 NB, to get the CI (95% by default) of LPOCV, use the followings:
```r
LPOCV.boot<-boot(data=df.mat.term, statistic=get_LPOCV_boot,R=100,parallel="multicore",ncpus=20) # R: the No. of resampling
LPOCV.ci<-boot.ci(LPOCV.boot)
```

## The core `R` script
The core `R` script is `run_glParallel.R` and make sure that the following packages are avaible from the R that you are using.
```r 
library(data.table) # iloveit
library(parallel)
library(doParallel) # for parallel jobs
library(foreach)
library(iterators)
library(magrittr)
library(pROC)
```

## Computation time  by different methods
4-predictor models were selected out of 17 available predictors, that makes a total of 2380 possible models to test. The following three methods were used using 32 cores in a single skylake computation node:
+ Method1: `parallel::mclapply` (via `parallel`'s fork)
```r
   user  system elapsed
  9.957   1.208   0.476
```
+ Method2: `foreach` and `doParallel` (via `parallel`'s fork)
```r
   user  system elapsed
 11.635   2.190   1.924
```
+ Method3: `parallel::parLapply` (via `parallel`'s socket)
```r
  user  system elapsed
  0.159   0.047   1.729
```

## To do
+ Properly packing it via `R` package
+ Streamline via workflow management tools such as [`SnakeMake`](https://github.com/snakemake/snakemake) or [`Nextflow`](https://www.nextflow.io/)

---
Last modified by Sung <@ssg29>
2023-05-04
