#!/bin/bash
# Author: Sung Gong <ssg29@cam.ac.uk>
# First created: 28/Feb/2018
# Last modified: 28/Feb/2018

# list modules to load
module load zlib-1.2.11-gcc-5.4.0-dmjwhms
module load parallel 
module load R/4.0.3
module load pandoc/2.0.6
module load texlive/2015
