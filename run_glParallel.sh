#!/bin/bash
# Author: Sung Gong <ssg29@cam.ac.uk>
# run `glm` in parallel, hence, glParallel
# First created: 2022-01-31
# Last modified: 2023-05-04

N_V=$1 # number of protein variables to be selected
N_C=$2 # number of chunk
N_X=$3 # index of chunk to run
METHOD=$4 # 1: mclapply (via fork), 2: foreach (via fork), 3: parLapply (via socket)

echo "Setting..."
#source ~/miniconda3/etc/profile.d/conda.sh # https://github.com/conda/conda/issues/8536
#conda activate bioc3.12

echo "Running..."

#echo -e "$HOME/miniconda3/envs/bioc3.12/bin/Rscript run_glParallel.R $N_V $N_C $N_X $METHOD"
#$HOME/miniconda3/envs/bioc3.12/bin/Rscript run_glParallel.R $N_V $N_C $N_X $METHOD

#Rscript run_glParallel.R 4 10 10 1
Rscript run_glParallel.R $N_V $N_C $N_X $METHOD

#conda deactivate

echo "Done"
