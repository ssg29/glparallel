#!/bin/bash
# Author: Sung Gong <ssg29@cam.ac.uk>
# run `glm` in parallel, hence, glParallel
# First created: 2022-01-31
# Last modified: 2023-05-05

export NAME="Pipeline1" #
export INPUT="data\/dummy.input.csv" #
export N_V=4 # NO. of features (e.g protein, gene, etc) to select
export N_C=1 # NO. of chunk
export METHOD=3 # 1: mclapply (via fork), 2: foreach (via fork), 3: parLapply (via socket)

MY_SLURM_TEMPLATE="slurm/slurm.skylake.glParallel.template.sh"

for N_X in $(seq 1 $N_C); 
do 
    MY_SLURM_SCRIPT=slurm/slurm.glParallel.$NAME.v$N_V.c$N_C.x$N_X.m$METHOD.sh
    cp $MY_SLURM_TEMPLATE $MY_SLURM_SCRIPT
    sed -i "s/MY_INPUT/$INPUT/" $MY_SLURM_SCRIPT
    sed -i "s/MY_NAME/$NAME/" $MY_SLURM_SCRIPT
    sed -i "s/MY_V/$N_V/" $MY_SLURM_SCRIPT
    sed -i "s/MY_C/$N_C/" $MY_SLURM_SCRIPT
    sed -i "s/MY_X/$N_X/" $MY_SLURM_SCRIPT
    sed -i "s/MY_M/$METHOD/" $MY_SLURM_SCRIPT

    echo -e "sbatch $MY_SLURM_SCRIPT";
    sbatch $MY_SLURM_SCRIPT 
done

# 5M for 15min 
# 10M for 35min 
# 20M for 1hr30min
